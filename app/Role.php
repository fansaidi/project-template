<?php

namespace App;

use Spatie\Permission\Models\Role as Model;

class Role extends Model
{
    const ADMIN = 'System Admin';
    const MODERATOR = 'Moderator';
    const CLIENT = 'Developer User';
    const DEVELOPER = 'IT Developer (for API)';
    const LAWFIRM = 'Law Firm User';
}
