<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::where('name', '!=', Role::ADMIN)->get();
        $permissions = Permission::all();

        $grouped_permissions = $this->groupPermission($permissions);

        return view('roles.index', [
            'roles' => $roles,
            'permissions' => $grouped_permissions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();

        $grouped_permissions = $this->groupPermission($permissions);

        return view('roles.create')->with('permissions', $grouped_permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate name and permissions field
        $this->validate(
            $request,
            [
                'name' => 'required|unique:roles|max:20'
            ]
        );

        $name = strtolower($request->name);
        $role = new Role();
        $role->name = $name;
        $role->email = $request->email;
        $role->description = $request->description;
        $role->save();

        if (isset($request->permissions)) {
            $permissions = $request->permissions;
            //Looping thru selected permissions
            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail();
                //Fetch the newly created role and assign permission
                $role = Role::where('name', '=', $name)->first();
                $role->givePermissionTo($p);
            }
        }

        return redirect()->route('roles.index')->with('status', 'Role ' . $role->name . ' added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

        $permissions = Permission::all();

        $grouped_permissions = $this->groupPermission($permissions);

        return view('roles.edit', [
            'role' => $role->load(['permissions']),
            'permissions' => $grouped_permissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {

        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $role->id,
        ]);

        $input = $request->except(['permissions']);
        $role->fill($input)->save();

        $p_all = Permission::all();

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p);
        }

        if (isset($request->permissions)) {
            $permissions = $request->permissions;
            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
                $role->givePermissionTo($p);  //Assign permission to role
            }
        }

        return redirect()->route('roles.index')
            ->with('status', 'Role ' . $role->name . ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('roles.index')
            ->with('status', 'Role ' . $role->name . ' deleted!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Spatie\Permission\Models\Permission $permissions
     * @return \Illuminate\Http\Response
     */
    public function groupPermission($permissions)
    {
        foreach ($permissions as $permission) {
            $p = explode('_', $permission->name, 2);
            $group = $p[0];
            $action = $p[1];
            $values = [
                'id' => $permission->id,
                'name' => $action,
                'value' => $permission->name
            ];
            $grouped_permissions[$group][] = $values;
        }

        return $grouped_permissions;
    }
}
