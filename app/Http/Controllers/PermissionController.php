<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::withCount('roles')->get();
        $roles = Role::where('name', '!=', Role::ADMIN)->get();

        return view('permissions.index', [
            'permissions' => $permissions,
            'roles' => $roles,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();

        return view('permissions.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:40',
        ]);

        $name = $request->name;
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request->roles;

        $permission->save();

        if (!empty($request['roles'])) {
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail();

                $permission = Permission::where('name', '=', $name)->first();
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('permissions.index')
            ->with('status', 'Permission ' . $permission->name . ' added!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {

        $roles = Role::where('name', '!=', Role::ADMIN)->get();

        return view('permissions.edit', [
            'permission' => $permission->load(['roles']),
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {

        $this->validate($request, [
            'name' => 'required|max:40',
        ]);
        $input = $request->only('name');
        $permission->fill($input)->save();

        if ($request->roles) {
            $permission->syncRoles($request->roles);
        } else {
            $permission->roles()->detach();
        }

        return redirect()->route('permissions.index')
            ->with('status', 'Permission ' . $permission->name . ' updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        //Make it impossible to delete this specific permission 
        if ($permission->name == "full_control") {
            return redirect()->route('permissions.index')
                ->with('status', 'Cannot delete this Permission!');
        }
        $permission->delete();

        return redirect()->route('permissions.index')
            ->with('status', 'Permission deleted!');
    }
}
