@extends('layouts.app', ['title' => 'Roles'])

@section('content')

@include('layouts.partials.header', ['title' => 'Manage Roles'])   
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Roles</h3>
                        </div>
                        <div class="col-4 text-right">
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-create-role">Add role</button>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        @if($roles->count() > 0)
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Email</th>
                                <th scope="col">Permissions</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td>
                                    {{ $role->name }}
                                </td>
                                <td>
                                    {{ $role->description }}
                                </td>
                                <td>
                                    {{ $role->email }}
                                </td>
                                <td>
                                    {{ $role->permissions->count() }}
                                </td>
                                <!-- Revoke Button -->
                                <td>
                                    <a href="{{ route('roles.edit', $role) }}" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Edit</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @else
                        <tr>
                            <td colspan="5">No role created.</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>

<!-- Create Role Modal -->
<div class="modal fade" id="modal-create-role" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Create Role
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <!-- Create Role Form -->
                <form id="add-role-form" role="form" method="POST" action="{{ route('roles.store') }}">
                    @csrf
                    <!-- Name -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Role Name</label>
                        <div class="col-md-10">
                            <input id="create-role-name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Email</label>
                        <div class="col-md-10">
                            <input id="create-role-description" type="text" class="form-control" name="email">
                        </div>
                    </div>
                    <!-- Description -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Description</label>
                        <div class="col-md-10">
                            <textarea id="create-role-description" type="text" class="form-control" name="description"></textarea>
                        </div>
                    </div>
                    <!-- Permission-->
                    <div class="form-row">
                        <label class="col-md-2 col-form-label">Permissions</label>
                        <div class="col-md-10">
                            @if(!empty($permissions))
                            @foreach($permissions as $key => $permission)
                            <div class="form-group">
                                <label class="form-label">{{ ucfirst($key) }}</label>
                                <div class="selectgroup selectgroup-pills">
                                    @foreach($permission as $action)
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="permissions[]" class="selectgroup-input" value="{{ $action['id'] }}">
                                        <span class="selectgroup-button">{{ ucfirst($action['name']) }}</span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                            @else
                            <p class="mb-0">No Permission created.</p>
                            @endif
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal Actions -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="add-role-form-submit" type="button" class="btn btn-primary">
                    Create
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Create Role Modal -->

@endsection

@push('js')
<script type="text/javascript">
    $('#add-role-form-submit').on('click', function(event) {
        $('#add-role-form').submit();
    });
</script>
@endpush