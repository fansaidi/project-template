@extends('layouts.app', ['title' => 'Edit Role'])

@section('content')

@include('layouts.partials.header', ['title' => 'Edit '.$role->name])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-body">
                    <form method="POST" action="{{route('roles.update',$role)}}">
                        @method('PUT')
                        @csrf
                        <!-- Role Name -->
                        <div class="form-row mb-2">
                            <label class="col-md-2 col-form-label">Role Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value='{{ $role->name }}'>
                            </div>
                        </div>
                        <!-- Role Email -->
                        <div class="form-row mb-2">
                            <label class="col-md-2 col-form-label">Role Email (For notification)</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="email" value='{{ $role->email }}'>
                            </div>
                        </div>
                        <!-- Role Description -->
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Description</label>
                            <div class="col-md-10">
                                <textarea id="create-role-description" type="text" class="form-control" name="description">{{ $role->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="col-md-2 col-form-label">Permissions</label>
                            <div class="col-md-10">
                                @if(!empty($permissions))
                                @foreach($permissions as $key => $permission)
                                <div class="form-group">
                                    <label class="form-label">{{ ucfirst($key) }}</label>
                                    <div class="selectgroup selectgroup-pills">
                                        @foreach($permission as $action)
                                        <label class="selectgroup-item">
                                            <input type="checkbox" name="permissions[]" class="selectgroup-input" value="{{ $action['id'] }}" {{ in_array($action['id'],$role->permissions->pluck('id')->toArray())?'checked':'' }}>
                                            <span class="selectgroup-button">{{ ucfirst($action['name']) }}</span>
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <p class="mb-0">No Permission created.</p>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <div>
                                <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                                    document.getElementById('delete-role-form').submit();">
                                    Delete
                                </button>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                    <form id="delete-role-form" action="{{ route('roles.destroy',$role) }}" method="POST" style="display: none;">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection