@extends('layouts.app', ['title' => 'Edit Permission'])

@section('content')

@include('layouts.partials.header', ['title' => 'Edit '.$permission->name])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{route('permissions.update',$permission)}}">
                        @method('PUT')
                        @csrf
                        <!-- Role Name -->
                        <div class="form-row mb-2">
                            <label class="col-md-2 col-form-label">Permission Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" value='{{ $permission->name }}'>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="col-md-2 col-form-label">Roles</label>
                            <div class="col-md-10">
                                @if($roles->count() > 0)
                                <div class="form-group">
                                    <div class="selectgroup selectgroup-pills">
                                        @foreach($roles as $role)
                                        <label class="selectgroup-item">
                                            <input type="checkbox" name="roles[]" class="selectgroup-input" value="{{ $role->id }}" {{ in_array($role->id,$permission->roles->pluck('id')->toArray())?'checked':'' }}>
                                            <span class="selectgroup-button">{{ ucfirst($role->name) }}</span>
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                                @else
                                <p class="mb-0">No Role created.</p>
                                @endif
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <div>
                                <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                                    document.getElementById('delete-permission-form').submit();">
                                    Delete
                                </button>
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form>
                    <form id="delete-permission-form" action="{{ route('permissions.destroy',$permission) }}" method="POST" style="display: none;">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection