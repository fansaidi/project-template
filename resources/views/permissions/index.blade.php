@extends('layouts.app', ['title' => 'Permissions'])

@section('content')
@include('layouts.partials.header', ['title' => 'Manage Permissions'])
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Permissions</h3>
                        </div>
                        <div class="col-4 text-right">
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-create-permission">Add permission</button>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        @if($permissions->count() > 0)
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Roles</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                            <tr>
                                <td>
                                    {{ $permission->name }}
                                </td>
                                <td>
                                    {{ $permission->roles_count }}
                                </td>
                                <!-- Revoke Button -->
                                <td>
                                    <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-primary btn-sm">
                                        <i class="far fa-edit"></i>
                                        Edit
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @else
                        <tr>
                            <td colspan="5">No permission created.</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footers.auth')
</div>

<!-- Create Role Modal -->
<div class="modal fade" id="modal-create-permission" tabindex="-1" role="dialog" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Create Permission
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <!-- Create Permission Form -->
                <form id="add-permission-form" role="form" method="POST" action="{{ route('permissions.store') }}">
                    @csrf
                    <!-- Name -->
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Permission Name</label>
                        <div class="col-md-10">
                            <input id="create-unit-name" type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <!-- Permission-->
                    <div class="form-row">
                        <label class="col-md-2 col-form-label">Roles</label>
                        <div class="col-md-10">
                            @if($roles->count() > 0)
                            <div class="form-group">
                                <div class="selectgroup selectgroup-pills">
                                    @foreach($roles as $role)
                                    <label class="selectgroup-item">
                                        <input type="checkbox" name="roles[]" class="selectgroup-input" value="{{ $role->id }}">
                                        <span class="selectgroup-button">{{ ucfirst($role->name) }}</span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                            @else
                            <p class="mb-0">No Role created.</p>
                            @endif
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal Actions -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="add-permission-form-submit" type="button" class="btn btn-primary">
                    Create
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    $('#add-permission-form-submit').on('click', function(event) {
        $('#add-permission-form').submit();
    });
</script>
@endpush