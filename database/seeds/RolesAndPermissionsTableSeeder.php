<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Role;

class RolesAndPermissionsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
        // Create permissions
        // 
        // Users permissions
        Permission::create(['name' => 'users_create']);
        Permission::create(['name' => 'users_view']);
        Permission::create(['name' => 'users_edit']);
        Permission::create(['name' => 'users_delete']);

        Permission::create(['name' => 'users_manage']);
        Permission::create(['name' => 'users_activate']);
        Permission::create(['name' => 'users_deactivate']);

        // Projects permissions
        Permission::create(['name' => 'projects_create']);
        Permission::create(['name' => 'projects_view']);
        Permission::create(['name' => 'projects_edit']);
        Permission::create(['name' => 'projects_delete']);

        Permission::create(['name' => 'projects_manage']);
        Permission::create(['name' => 'projects_view_all']);

        // Templates permissions   
        Permission::create(['name' => 'templates_create']);
        Permission::create(['name' => 'templates_view']);
        Permission::create(['name' => 'templates_edit']);
        Permission::create(['name' => 'templates_delete']);

        Permission::create(['name' => 'templates_manage']);

        // Units permissions  
        Permission::create(['name' => 'units_create']);
        Permission::create(['name' => 'units_view']);
        Permission::create(['name' => 'units_edit']);
        Permission::create(['name' => 'units_delete']);

        Permission::create(['name' => 'units_manage']);
        Permission::create(['name' => 'units_view_all']);
        Permission::create(['name' => 'units_import']);
        Permission::create(['name' => 'units_purchaser_add']);

        // Orders permissions
        Permission::create(['name' => 'orders_create']);
        Permission::create(['name' => 'orders_view']);
        Permission::create(['name' => 'orders_edit']);
        Permission::create(['name' => 'orders_delete']);

        Permission::create(['name' => 'orders_manage']);

        // Purchasers permissions
        Permission::create(['name' => 'purchasers_create']);
        Permission::create(['name' => 'purchasers_view']);
        Permission::create(['name' => 'purchasers_edit']);
        Permission::create(['name' => 'purchasers_delete']);

        Permission::create(['name' => 'purchasers_manage']);
        Permission::create(['name' => 'purchasers_view_all']);

        // Agreements permisions
        Permission::create(['name' => 'agreements_create']);
        Permission::create(['name' => 'agreements_view']);
        Permission::create(['name' => 'agreements_edit']);
        Permission::create(['name' => 'agreements_delete']);

        Permission::create(['name' => 'agreements_manage']);
        Permission::create(['name' => 'agreements_signed_upload']);
        Permission::create(['name' => 'agreements_signed_download']);

        // Estamps permisions
        Permission::create(['name' => 'estamps_create']);
        Permission::create(['name' => 'estamps_view']);
        Permission::create(['name' => 'estamps_edit']);
        Permission::create(['name' => 'estamps_delete']);

        Permission::create(['name' => 'estamps_manage']);
        Permission::create(['name' => 'estamps_upload']);
        Permission::create(['name' => 'estamps_download']);

        // Agents permisssions
        Permission::create(['name' => 'agents_create']);
        Permission::create(['name' => 'agents_view']);
        Permission::create(['name' => 'agents_edit']);
        Permission::create(['name' => 'agents_delete']);

        Permission::create(['name' => 'agents_manage']);

        // Roles permisssions
        Permission::create(['name' => 'roles_create']);
        Permission::create(['name' => 'roles_view']);
        Permission::create(['name' => 'roles_edit']);
        Permission::create(['name' => 'roles_delete']);

        Permission::create(['name' => 'roles_manage']);

        // Permissions permisssions
        Permission::create(['name' => 'permissions_create']);
        Permission::create(['name' => 'permissions_view']);
        Permission::create(['name' => 'permissions_edit']);
        Permission::create(['name' => 'permissions_delete']);

        Permission::create(['name' => 'permissions_manage']);

        // API permisssions        
        Permission::create(['name' => 'api_manage']);
        Permission::create(['name' => 'api_access_token_create']);

        // Audits permission
        Permission::create(['name' => 'audits_view']);
        
        // Letters permisssions        
        Permission::create(['name' => 'letters_create']);
        Permission::create(['name' => 'letters_delete']);
        
        // Create admin roles and assign existing permissions
        // Admin has all access
        $role = Role::create(['name' => Role::ADMIN]);

        // Create moderator roles and assign existing permissions
        $role = Role::create(['name' => Role::MODERATOR]);
        $role->givePermissionTo('users_manage');
        $role->givePermissionTo('users_create');
        $role->givePermissionTo('users_activate');
        $role->givePermissionTo('users_deactivate');
        $role->givePermissionTo('templates_manage');
        $role->givePermissionTo('templates_create');
        $role->givePermissionTo('projects_manage');
        $role->givePermissionTo('projects_create');
        $role->givePermissionTo('projects_view');
        $role->givePermissionTo('projects_view_all');
        $role->givePermissionTo('projects_delete');
        $role->givePermissionTo('units_manage');
        $role->givePermissionTo('units_create');
        $role->givePermissionTo('units_view');
        $role->givePermissionTo('units_view_all');
        $role->givePermissionTo('units_edit');
        $role->givePermissionTo('units_import');
        $role->givePermissionTo('units_delete');
        $role->givePermissionTo('estamps_manage');
        $role->givePermissionTo('estamps_upload');
        $role->givePermissionTo('estamps_download');
        $role->givePermissionTo('audits_view');
        $role->givePermissionTo('letters_create');
        $role->givePermissionTo('letters_delete');
        $role->givePermissionTo('agreements_signed_download');

        // Create client(property developer) roles and assign existing permissions
        $role = Role::create(['name' => Role::CLIENT]);
        $role->givePermissionTo('projects_manage');
        $role->givePermissionTo('projects_view');
        $role->givePermissionTo('units_manage');
        $role->givePermissionTo('units_view');
        $role->givePermissionTo('agents_manage');
        $role->givePermissionTo('agreements_manage');
        $role->givePermissionTo('agreements_signed_upload');
        $role->givePermissionTo('orders_manage');
        $role->givePermissionTo('orders_create');

        // Create lawfirm roles and assign existing permissions
        $role = Role::create(['name' => Role::LAWFIRM]);
        $role->givePermissionTo('projects_manage');
        $role->givePermissionTo('projects_view');
        $role->givePermissionTo('units_manage');
        $role->givePermissionTo('units_view');
        $role->givePermissionTo('agents_manage');
        $role->givePermissionTo('agreements_manage');
        $role->givePermissionTo('agreements_signed_upload');
        $role->givePermissionTo('orders_manage');
        $role->givePermissionTo('orders_create');

        // Create developer roles and assign existing permissions
        $role = Role::create(['name' => Role::DEVELOPER]);
        $role->givePermissionTo('api_manage');
        $role->givePermissionTo('api_access_token_create');
    }

}